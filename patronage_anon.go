package main

import (
	"bufio"
	"fmt"
	"github.com/subosito/twilio"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

/**
 * Message type enum
 */
const (
	HELP = iota
	NO_ACCOUNT
	REGISTER_SUCCESS
	REGISTER_FAILURE
	REQ_OFFER
	REQ_WAIT
	REQ_CONFIRM
	REQ_SUCCESS
	REQ_FAILURE
	PAT_REGISTER_SUCCESS
	PAT_REGISTER_FAILURE
	PAT_CANCEL_SUCCESS
	PAT_CANCEL_FAILURE
	PAT_OFFER
	PAT_SUCCESS
	PAT_FAILURE
)

/**
 * Account state enum
 */
const (
	IDLE = iota
	OCCUPIED
	WAITING
	NO_STATE
)

/**
 * Request type enum
 */
const (
	SAYLES = iota
	PRINT
)

/**
 * Other constants
 */
const (
	MAX_ACCOUNT_NAME_LEN        = 15
	MAX_DESCRIPTION_LEN         = 18
	MAX_OFFERS_PER_WEEK         = 1000
	accountsFile         string = "patronage_accounts.csv"
	patronsFile          string = "patronage_patrons.csv"
	AccountSid                  = "YOUR_ID_HERE"
	AuthToken                   = "YOUR_TOKEN_HERE"
	twilioFrom                  = "YOUR_TWILIO_NUMBER_HERE"
)

var (
	// The existing messages have a 68 character limit.
	// It would be wonderful if we had more characters to use.
	// (We can probably go up to 80)
	textMessages = map[int]string{
		HELP:                 "Commands: SAYLES or PRINT request, become a PATRON, DISABLE patronage",
		NO_ACCOUNT:           "You don't have an account! (Text REGISTER with your name)",
		REGISTER_SUCCESS:     "Your account: %s. Ensure this is your real name",
		REGISTER_FAILURE:     "Failed. Text REGISTER followed by your name (< " + strconv.Itoa(MAX_ACCOUNT_NAME_LEN) + " char) to register",
		REQ_OFFER:            "Success! Your request remains til it is fulfilled, or for 24 hours",
		REQ_WAIT:             "You already have an outstanding %s request.",
		REQ_CONFIRM:          "%s has taken your %s request. YES or NO?",
		REQ_SUCCESS:          "%s request accepted. Your patron's number is %s",
		REQ_FAILURE:          "Failed. Text %s followed by a description (< " + strconv.Itoa(MAX_DESCRIPTION_LEN) + "char) to request",
		PAT_REGISTER_SUCCESS: "You are now a patron. You will receive at most %s offers/week.",
		PAT_REGISTER_FAILURE: "Failed. Text PATRON followed by # of offers/week you'd like to get.",
		PAT_CANCEL_SUCCESS:   "Success! You are no longer a patron (even if you weren't before)",
		PAT_CANCEL_FAILURE:   "Failed cancellation. Send a report to carl.patronage@gmail.com",
		PAT_OFFER:            "%s request from %s: %s. YES/NO?",
		PAT_SUCCESS:          "Patronage accepted. Your contact's number is %s.",
		PAT_FAILURE:          "Your recipient has changed their mind about their request.",
	}
	typeStrings = []string{
		"Sayles",
		"Print",
	}
	// We're using maps when we should be using a database
	// It'll get changed eventually
	accounts       = make(map[string]Account)
	patrons        = make(map[string]Patron)
	requests       = [2]map[int64]string{{}, {}}
	activeRequests = [2]map[int64]string{{}, {}}
	patronPairings = make(map[string]chan bool)
	// Message channels for passing
	// information between goroutines
	newSaylesRequestChan = make(chan string)
	newPrintRequestChan  = make(chan string)
	newRequestChannels   = [2]chan string{newSaylesRequestChan, newPrintRequestChan}
	newPatronChan        = make(chan string)
	outgoingSMSChan      = make(chan bool)
	messageChan          = make(chan *ReceivedMessage)
	// For generating new requests.
	// Would be made unnecessary with a good database.
	nextRequestID int64 = 1

	// The client handles outgoing sms requests.
	twilioClient = twilio.NewClient(AccountSid, AuthToken, nil)
)

/**
 * Data types
 * A request data type might be necessary in the future.
 * These data types rely on being accessed via maps.
 * They implicitly have string IDs.
 * Patrons all have a matching account with the same ID.
 */
type Account struct {
	name     string
	state    int
	requests [2]int64
}

type Patron struct {
	maxRequests, requestsTaken int
}

type ReceivedMessage struct {
	MSISDN  string
	Keyword string
	Text    string
}

/**
 * A helper for in-lining Atoi
 */
func g_atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

/**
 * This receives receipts for texts and logs them.
 */
func receipt(w http.ResponseWriter, r *http.Request) {
	fmt.Print("\n", r, "\n\n")
}

/**
 * Return an account's state, or if the account does not
 * exist, return no state.
 */
func getState(id string) int {
	ac, ok := accounts[id]
	if !ok {
		return NO_STATE
	}
	return ac.state
}

/**
 * Helper to increment and return the id associated
 * with a given queue item
 */
func addRequestToStringQueue(s string, m map[int64]string) int64 {
	nextRequestID++
	m[nextRequestID] = s
	fmt.Println(m)
	return nextRequestID
}

func makeRequest(msg *ReceivedMessage, ty int) {
	desc := msg.Text
	id := addRequestToStringQueue(desc, requests[ty])

	if len(desc) > MAX_DESCRIPTION_LEN {
		desc = desc[0:MAX_DESCRIPTION_LEN]
	}

	ac := accounts[msg.MSISDN]
	if ac.requests[ty] != -1 {
		SMSReturn(msg.MSISDN, REQ_WAIT, typeStrings[ty])
		return
	}

	ac.requests[ty] = id
	accounts[msg.MSISDN] = ac

	fmt.Println(accounts[msg.MSISDN])

	SMSReturn(msg.MSISDN, REQ_OFFER)

	time.AfterFunc(time.Hour*24, func() {
		if ac.requests[ty] == id {
			ac.requests[ty] = -1
		}
	})

	newRequestChannels[ty] <- msg.MSISDN
}

func patronRegister(msg *ReceivedMessage) {
	s_limit := msg.Text
	limit, err := strconv.Atoi(s_limit)

	if err != nil {
		SMSReturn(msg.MSISDN, PAT_REGISTER_FAILURE)
		return
	}

	if limit > MAX_OFFERS_PER_WEEK {
		limit = MAX_OFFERS_PER_WEEK
	}

	pt, alreadyPatron := patrons[msg.MSISDN]
	if alreadyPatron {
		pt.maxRequests = limit
		patrons[msg.MSISDN] = pt
	} else {
		patrons[msg.MSISDN] = Patron{limit, 0}
	}

	fmt.Println(patrons)

	//TODO: Write to patrons file

	SMSReturn(msg.MSISDN, PAT_REGISTER_SUCCESS, strconv.Itoa(limit))
	if !alreadyPatron {
		newPatronChan <- msg.MSISDN
	}
}

func patronUnregister(msg *ReceivedMessage) {

	pt, ok := patrons[msg.MSISDN]
	if ok {
		pt.maxRequests = 0
		patrons[msg.MSISDN] = pt
		time.AfterFunc(time.Hour*24, func() {
			delete(patrons, msg.MSISDN)
		})
	}

	SMSReturn(msg.MSISDN, PAT_CANCEL_SUCCESS)
}

func register(msg *ReceivedMessage) {
	name := msg.Text

	if name == "" {
		SMSReturn(msg.MSISDN, REGISTER_FAILURE)
		return
	}

	if len(name) > MAX_ACCOUNT_NAME_LEN {
		name = name[0:MAX_ACCOUNT_NAME_LEN]
	}

	accounts[msg.MSISDN] = Account{
		name,
		IDLE,
		[2]int64{-1, -1},
	}

	fmt.Println(accounts)

	//TODO: Write to accounts file

	SMSReturn(msg.MSISDN, REGISTER_SUCCESS, name)
}

func SMSReturn(id string, messageType int, formVals ...interface{}) {

	<-outgoingSMSChan

	textMessage := textMessages[messageType]
	if len(formVals) > 0 {
		textMessage = fmt.Sprintf(textMessage, formVals...)
	}

	fmt.Println("Sending: ", textMessage)

	s, resp, err := twilioClient.Messages.SendSMS(twilioFrom, id, textMessage)
	log.Println("Send:", s)
	log.Println("Response:", resp)
	log.Println("Err:", err)
}

func loadAccounts(file string) map[string]Account {
	acf := make(map[string]Account)
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	r := bufio.NewReader(f)
	for true {
		line, err := r.ReadString('\n')

		fmt.Println(line, err)
		fields := strings.FieldsFunc(line, func(r rune) bool { return r == ',' })
		if _, ok := acf[fields[0]]; ok {
			log.Fatal("Duplicate account records in accounts file found.")
		}
		acf[fields[0]] = Account{
			fields[1],
			IDLE,
			[2]int64{-1, -1},
		}

		if err != nil {
			break
		}
	}
	return acf
}

func loadPatrons(file string) map[string]Patron {
	ptf := make(map[string]Patron)
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	r := bufio.NewReader(f)
	for true {
		line, err := r.ReadString('\n')

		fields := strings.FieldsFunc(line, func(r rune) bool { return r == ',' })
		if _, ok := ptf[fields[0]]; ok {
			log.Fatal("Duplicate patron records in patrons file found.")
		}
		ptf[fields[0]] = Patron{
			g_atoi(fields[1]),
			0,
		}

		if err != nil {
			break
		}
	}
	return ptf
}

/*
* Wait on a given boolean channel for ten minutes.
* If the channel does not respond in that time, return false.
 */
func waitOnChannel(c chan bool) bool {
	select {
	case response := <-c:
		return response
	case <-time.After(time.Minute * 10):
		return false
	}
}

func patronage(w http.ResponseWriter, r *http.Request) {
	fmt.Print("\n", r, "\n\n")

	r.ParseForm()

	fmt.Println(r.FormValue("To"))
	fmt.Println(r.FormValue("From"))
	fmt.Println(r.FormValue("Body"))

	s := r.FormValue("Body")
	var text string

	splitText := strings.SplitN(s, " ", 2)
	if len(splitText) < 2 || splitText[1] == "" {
		text = ""
	} else {
		text = splitText[1]
	}

	messageChan <- &ReceivedMessage{
		MSISDN:  r.FormValue("From"),
		Keyword: strings.ToUpper(splitText[0]),
		Text:    text,
	}
}

func main() {
	fmt.Println("Loading Patronage.")

	accounts = loadAccounts(accountsFile)
	patrons = loadPatrons(patronsFile)

	fmt.Println("Loaded Accounts.")
	fmt.Println("accounts", accounts)
	fmt.Println("patrons", patrons)
	fmt.Println("Loading Services.")

	// Primary service
	// Parses messages as commands and matches
	// commands to functions
	go func() {
		for {
			msg := <-messageChan
			log.Printf("%v\n", msg)
			state := getState(msg.MSISDN)
			switch state {
			case IDLE:
				switch msg.Keyword {
				case "PR":
					fallthrough
				case "PRINT":
					go makeRequest(msg, PRINT)
				case "PA":
					fallthrough
				case "PATRON":
					go patronRegister(msg)
				case "R":
					fallthrough
				case "REGISTER":
					go register(msg)
				case "S":
					fallthrough
				case "SAYLES":
					go makeRequest(msg, SAYLES)
				case "D":
					fallthrough
				case "DISABLE":
					go patronUnregister(msg)
				case "H":
					fallthrough
				case "HELP":
					fallthrough
				default:
					go SMSReturn(msg.MSISDN, HELP)
				}
			// In the occupied state, the account
			// is waiting on a yes or no response
			// to a given query. The account has
			// a channel associated with that
			// query in patronPairings to which
			// the response is sent.
			case OCCUPIED:
				switch msg.Keyword {
				case "Y":
					fallthrough
				case "YES":
					patronPairings[msg.MSISDN] <- true
				case "N":
					fallthrough
				case "NO":
					patronPairings[msg.MSISDN] <- false
				}
			// When no account for a number exists,
			// require that the user register.
			case NO_STATE:
				switch msg.Keyword {
				case "R":
					fallthrough
				case "REGISTER":
					go register(msg)
				default:
					go SMSReturn(msg.MSISDN, NO_ACCOUNT)
				}
			}
		}
	}()

	// When new requests are made, and when new patrons
	// are assigned, we attempt to pass around the
	// new item to assure it gets interacted with by
	// existing patrons / requests.
	go func() {
		for {
			select {
			case patronId := <-newPatronChan:
				go distributeNewPatron(patronId)
			case accountId := <-newRequestChannels[SAYLES]:
				go distributeNewRequest(accountId, SAYLES)
			case accountId := <-newRequestChannels[PRINT]:
				go distributeNewRequest(accountId, PRINT)
			}
			time.Sleep(time.Second * 5)
		}
	}()

	// Once per second, allow an outgoing sms to go through.
	go func() {
		for {
			time.Sleep(time.Millisecond * 1100)
			outgoingSMSChan <- true
		}
	}()

	// Once per week, reset all patrons' taken request counts
	go func() {
		for {
			time.Sleep(time.Hour * 24 * 7)
			for i, p := range patrons {
				p.requestsTaken = 0
				patrons[i] = p
			}
		}
	}()

	fmt.Println("Loading Client.")

	// Set up our routes
	http.HandleFunc("/patron/receipt", receipt)
	http.HandleFunc("/patron/sms", patronage)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal("PatronServe: ", err)
	}
}

func distributeNewPatron(pid string) {
	pt := patrons[pid]
	for i, ac := range accounts {
		if _, ok := activeRequests[SAYLES][ac.requests[SAYLES]]; !ok {
			if ac.requests[SAYLES] != -1 {
				success := communicateRequest(ac, pt, i, pid, SAYLES)
				if success {
					break
				}
			}
		} else if _, ok := activeRequests[PRINT][ac.requests[PRINT]]; !ok {
			if ac.requests[PRINT] != -1 {
				success := communicateRequest(ac, pt, i, pid, PRINT)
				if success {
					break
				}
			}
		}
	}
}

func communicateRequest(ac Account, p Patron, accountId, pid string, ty int) bool {
	pt := accounts[pid]
	if pid != accountId && p.maxRequests > p.requestsTaken &&
		pt.state == IDLE && ac.state == IDLE {
		pt.state = OCCUPIED
		accounts[pid] = pt

		transactionChan := make(chan bool)
		patronPairings[pid] = transactionChan
		patronPairings[accountId] = transactionChan

		// Send to Patron inital offer
		SMSReturn(pid, PAT_OFFER, typeStrings[ty], ac.name, requests[ty][ac.requests[ty]])

		response := waitOnChannel(transactionChan)
		if response {
			// Send to Recipient initial offer
			pt.state = WAITING
			accounts[pid] = pt

			ac.state = OCCUPIED
			accounts[accountId] = ac

			SMSReturn(accountId, REQ_CONFIRM, pt.name, typeStrings[ty])

			response = waitOnChannel(transactionChan)
			if !response {
				// Send to Patron rejection notice
				SMSReturn(pid, PAT_FAILURE)
			} else {
				// Send acceptance notices
				SMSReturn(pid, PAT_SUCCESS, accountId)
				SMSReturn(accountId, REQ_SUCCESS, typeStrings[ty], pid)
				patron := patrons[pid]
				patron.requestsTaken++
				patrons[pid] = patron
			}

			ac.state = IDLE
			ac.requests[ty] = -1
			accounts[accountId] = ac

			pt.state = IDLE
			accounts[pid] = pt

			delete(patronPairings, pid)
			delete(patronPairings, accountId)

			return response
		}
	}
	return false
}

func distributeNewRequest(accountId string, ty int) {
	ac := accounts[accountId]
	requestId := ac.requests[ty]
	requestDesc := requests[ty][requestId]

	activeRequests[ty][requestId] = requestDesc
	for pid, p := range patrons {
		success := communicateRequest(ac, p, accountId, pid, ty)
		if success {
			break
		}
	}

	delete(activeRequests[ty], requestId)
}
